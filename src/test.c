#include <stdio.h>
#include <inttypes.h>
#include "mem.h"
#include "test.h"

static void test_1() {
    printf("Обычное успешное выделение памяти:\n");

    void *a = _malloc(2);
    void *b = _malloc(500);
    void *c = _malloc(1600);
    debug_heap(stdout,HEAP_START);
    _free(a);
    _free(b);
    _free(c);

    printf("\n");
}

static void test_2() {
    printf("Освобождение одного блока из нескольких выделенных:\n");
    void *a = _malloc(2);
    void *b = _malloc(500);
    void *c = _malloc(1600);
    debug_heap(stdout, HEAP_START);
    _free(a);
    debug_heap(stdout,HEAP_START);
    _free(b);
    _free(c);

    printf("\n");
}

static void test_3() {
    printf("Освобождение двух блоков из нескольких выделенных:\n");
    void *a = _malloc(10);
    void *b = _malloc(25);
    void *c = _malloc(1000);
    debug_heap(stdout, HEAP_START);
    _free(b);
    _free(a);
    debug_heap(stdout, HEAP_START);
    _free(c);

    printf("\n");
}

static void test_4() {
    printf("Память закончилась, новый регион памяти расширяет старый:\n");
    debug_heap(stdout, HEAP_START);
    void* a_1 = _malloc(10000);
    debug_heap(stdout, HEAP_START);
    _free(a_1);
    debug_heap(stdout, HEAP_START);

    printf("\n");
}

static void test_5() {
    printf("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте:\n");
    debug_heap(stdout, HEAP_START);
    void* a_1 = _malloc(100000);
    debug_heap(stdout, HEAP_START);
    _free(a_1);
    debug_heap(stdout, HEAP_START);

}

void testss() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}
